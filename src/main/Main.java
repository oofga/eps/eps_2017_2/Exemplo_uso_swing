package main;

import java.io.IOException;

import javax.swing.JFrame;

import view.InterfaceGraficaInicial;


public class Main {
	public static void main(String [] args) throws IOException{
		JFrame telaInicial = new JFrame();
		telaInicial = new JFrame("Minha lista de filmes");
		telaInicial.setSize(1000, 500);
		telaInicial.setLocationRelativeTo(null);
		telaInicial.setResizable(false);
		new InterfaceGraficaInicial(telaInicial);
		
		
	}
}
